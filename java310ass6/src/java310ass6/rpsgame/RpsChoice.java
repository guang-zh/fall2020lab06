// Guang Kun Zhang
// 1942372
// https://gitlab.com/guang-zh/fall2020lab06.git

package rpsgame;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
//	private String message;
//	private String wins;
//	private String losses;
//	private String ties;
	private String playerChoice;
	private RpsGame rpg;
//	private String messages;
	
//	private Scene scene;
	private TextField tMessage;
	private TextField tWins;
	private TextField tLosses;
	private TextField tTies;
	
	
	
	public RpsChoice(String message, String wins, String losses, String ties, String playerChoice, RpsGame rpg) {
//		this.message = message;
//		this.wins = wins;
//		this.losses = losses;
//		this.ties = ties;
		this.playerChoice = playerChoice;
		this.rpg = rpg;
	}
	
	@Override
	public void handle(ActionEvent e) {
//		String messages = rpg.playRound(playerChoice);
		tMessage = new TextField(rpg.playRound(playerChoice));
//		tMessage.setText(rpg.playRound(playerChoice));
		tWins.setText("wins: "+rpg.getWins());
		tLosses.setText("losses: "+rpg.getLosses());
		tTies.setText("ties: "+rpg.getTies());
		
	}
}
