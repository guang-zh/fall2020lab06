// Guang Kun Zhang
// 1942372
// https://gitlab.com/guang-zh/fall2020lab06.git

package rpsgame;

public class RpsGameMain {
//	public Random rand;

	public static void main(String[] args) {
		
		RpsGame game = new RpsGame();
		System.out.println(game.playRound("rock"));
		System.out.println(game.playRound("scissors"));
		System.out.println(game.playRound("paper"));
		
		System.out.println();
		System.out.println("Wins: " + game.getWins());
		System.out.println("Loses: "+ game.getLosses());
		System.out.println("Ties: "+ game.getTies());

	}

}
