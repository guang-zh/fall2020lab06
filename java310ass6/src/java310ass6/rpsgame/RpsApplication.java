// Guang Kun Zhang
// 1942372
// https://gitlab.com/guang-zh/fall2020lab06.git
package rpsgame;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	private RpsGame rpg;
	public void start(Stage stage) {
		rpg = new RpsGame();
		Group root = new Group(); 
		VBox vbox = new VBox();
		HBox buttons = new HBox();
		Button rockButton = new Button("rock");
		Button scissorsButton = new Button("scissors");
		Button paperButton = new Button("paper");	
		buttons.getChildren().addAll(rockButton, scissorsButton, paperButton);
		
	
		HBox textFields = new HBox();
		TextField tMessage = new TextField("Welcome!");
		tMessage.setPrefWidth(200);
		TextField tWins = new TextField("wins: 0");
		tWins.setPrefWidth(200);
		TextField tLosses = new TextField("losses: 0");
		tLosses.setPrefWidth(200);
		TextField tTies = new TextField("ties: 0");
		tTies.setPrefWidth(200);
		textFields.getChildren().addAll(tMessage, tWins, tLosses, tTies);		
		
		RpsChoice rockActionObject = new RpsChoice(tMessage.getText(),tWins.getText(),tLosses.getText(),tTies.getText(),"rock", rpg);
		rockButton.setOnAction(rockActionObject);
		RpsChoice scissorsActionObject = new RpsChoice(tMessage.getText(),tWins.getText(),tLosses.getText(),tTies.getText(),"scissors", rpg);
		scissorsButton.setOnAction(scissorsActionObject);
		RpsChoice paperActionObject = new RpsChoice(tMessage.getText(),tWins.getText(),tLosses.getText(),tTies.getText(),"paper", rpg);
		paperButton.setOnAction(paperActionObject);
		
		
		vbox.getChildren().addAll(buttons, textFields);
		root.getChildren().add(vbox);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 800, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}

