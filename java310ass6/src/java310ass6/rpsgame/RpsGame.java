// Guang Kun Zhang
// 1942372
// https://gitlab.com/guang-zh/fall2020lab06.git
package rpsgame;

import java.util.Random;

public class RpsGame {
	private int wins=0;
	private int ties=0;
	private int losses=0;
	
	public Random rand;
	
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public String playRound(String input) {
		String msg = "";
		this.rand = new Random();
		int index = rand.nextInt(3);
//		Computer generate random int for the choices in order
		String[] choices = {"rock","scissors","paper"};
		String computer = choices[index];
// Guang 1942372		
		if (index == 0) {
			if (input.equals("scissors")) {
				msg = "Computer plays rock and the computer won";
				this.wins++;
			} else if (input.equals("paper")) {
				msg = "Computer plays rock and the computer lost";
				this.losses++;
			} else {
				msg = "Computer plays rock and it is a tie";
				this.ties++;
			} 
		}else if (index == 1){
			if (input.equals("scissors")) {
				msg = "Computer plays scissors and it is a tie";
				this.ties++;
			} else if (input.equals("paper")) {
				msg = "Computer plays scissors and the computer won";
				this.losses++;
			} else {
				msg = "Computer plays scissors and the computer lost";
				this.wins++;
			} 
		} else if (index==2) {
			if (input.equals("scissors")) {
				msg = "Computer plays paper and the computer lost";
				this.wins++;
			} else if (input.equals("paper")) {
				msg = "Computer plays paper and it is a tie";
				this.ties++;
			} else {
				msg = "Computer plays paper and the computer won";
				this.losses++;
			} 
	
		} else {
			msg = "Error! Check input to be one of {rock, scissors, paper}";
		}
		
		return msg;
			
	}

}
