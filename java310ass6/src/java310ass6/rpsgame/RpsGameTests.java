package rpsgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RpsGameTests {

	@Test
	void testGet() {
		int expectedWins=0;
		RpsGame game1 = new RpsGame();
		int wins=game1.getWins();
		assertEquals(expectedWins, wins);
	}

}

